module.exports = {
  extends: ['airbnb', 'prettier'],

  env: {
    browser: true,
    node: true
  },

  globals: {
    // Jest / Enzyme
    jest: true,
    mount: true,
    render: true,
    shallow: true,

    // Jest ~ functions
    afterAll: false,
    afterEach: false,
    beforeAll: false,
    beforeEach: false,
    describe: false,
    expect: false,
    it: false,

    // i18n-webpack-plugin
    i18n: true
  },

  parser: 'babel-eslint',

  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 6,
    sourceType: 'module'
  },

  plugins: ['react', 'import', 'prettier'],

  rules: {
    'array-callback-return': 0,
    'arrow-parens': 0,
    'class-methods-use-this': 0,
    'comma-dangle': ['error', 'never'],
    'consistent-return': [
      'warn',
      {
        treatUndefinedAsUnspecified: false
      }
    ],

    'no-underscore-dangle': [
      'error',
      {
        allow: ['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__']
      }
    ],

    'function-paren-newline': ['warn', 'consistent'],
    'implicit-arrow-linebreak': 0,
    'import/prefer-default-export': 0,
    'function-paren-newline': 0,
    indent: [2, 2, { SwitchCase: 1 }],
    'jsx-a11y/label-has-for': [
      2,
      {
        required: {
          every: ['nesting', 'id']
        },
        allowChildren: true
      }
    ],

    'no-restricted-globals': ['error', 'event', 'fdescribe'],
    'prettier/prettier': 2,
    quotes: ['warn', 'single', { avoidEscape: true }],
    'react/no-unescaped-entities': 0,
    'react/jsx-one-expression-per-line': 0,
    'react/require-default-props': 0,
    "react/prop-types": 0,
    'react/sort-comp': [
      1,
      {
        order: [
          'instance-variables',
          'static-methods',
          '/^on.+$/',
          'lifecycle',
          'everything-else',
          'render'
        ]
      }
    ],

    'jsx-a11y/anchor-is-valid': ['error'],
    'jsx-a11y/no-static-element-interactions': 0,
    'import/no-extraneous-dependencies': 0,
    'import/first': 0,
    'jsx-a11y/anchor-is-valid': 0,
    'import/extensions': [
      'error',
      'never',
      {
        css: 'always',
        json: 'always'
      }
    ],

    'import/order': 0,
    'new-cap': [
      'error',
      {
        capIsNew: false,
        capIsNewExceptions: ['CSSModules', 'Schema', 'React'],
        newIsCapExceptions: ['events']
      }
    ],

    'object-curly-newline': [
      'warn',
      {
        ExportDeclaration: {
          consistent: true,
          multiline: true
          // minProperties: 3
        },
        ImportDeclaration: {
          consistent: true,
          multiline: true
          // minProperties: 3
        },
        ObjectExpression: {
          consistent: true,
          multiline: false
        },
        ObjectPattern: {
          consistent: true,
          multiline: false
        }
      }
    ],

    'padded-blocks': [
      'error',
      {
        blocks: 'never',
        classes: 'never',
        switches: 'never'
      }
    ],

    'prefer-arrow-callback': [
      'error',
      {
        allowNamedFunctions: true
      }
    ],

    'react/jsx-curly-spacing': [
      2,
      'never',
      {
        allowMultiline: true
      }
    ],

    'react/jsx-props-no-spreading': 0,
    'react/no-danger': 1,
    'react/jsx-filename-extension': [
      1,
      {
        extensions: ['.js', '.jsx']
      }
    ],

    'react/jsx-closing-bracket-location': [
      'error',
      {
        nonEmpty: 'tag-aligned',
        selfClosing: 'tag-aligned'
      }
    ],

    'space-before-function-paren': 0,
    'space-in-parens': ['error', 'never'],
    'template-curly-spacing': ['error', 'never'],
    'import/no-unresolved': 'off'
  }
};
