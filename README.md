## Starting the application
To start the application run next commands
```
yarn install
yarn start
```

## Architecture
The app is built using `create-react-app`, `redux`, `redux-saga`, `connected-react-router`. It uses `loadable-components` for loading asynchronous chunks of the code, `jest` and `react-resting-library` for testing.

The consists of the multiple `services` which of those is responsible for a particular piece of the functionality. For example `api` service, `form` service, `auth` service, etc.

For making API calls the app uses `api` service which listens for particular redux actions being dispatched, and executes a query for them. After the query is finished the service dispatches success or fail action with the respective payload attached. The service also takes care of auth cookies, CSRF protection etc.

Forms are implemented in `form` service which provides conveniece component such as `FormInput`, `FormSelect` etc, and a `useForm` hook which can be used to manage fields values, validations, handling submitted, touched and other states, passing correct values to the submit function etc. All the values are stored in the redux store.