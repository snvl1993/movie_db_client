import queryString from 'query-string';
import compact from 'lodash/compact';
import reduce from 'lodash/reduce';
import isArray from 'lodash/isArray';
import set from 'lodash/set';

const QUERY_STRING_DEFAULT_OPTIONS = { arrayFormat: 'bracket' };

const parseQueryString = (query) =>
  queryString.parse(query, QUERY_STRING_DEFAULT_OPTIONS);

const buildQueryString = (data) => {
  const compactData = reduce(
    data,
    (acc, v, k) => {
      const newValue = isArray(v) ? compact(v) : v;
      return !newValue ? acc : set(acc, k, newValue);
    },
    {}
  );

  return `?${queryString.stringify(compactData, QUERY_STRING_DEFAULT_OPTIONS)}`;
};

export { parseQueryString, buildQueryString };
