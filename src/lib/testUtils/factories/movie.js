import { createCategory } from './category';

const createMovie = (id, attributes = {}) => ({
  id,
  title: `Movie ${id}`,
  text: `Movie ${id} Text`,
  categories: [1, 2, 3].map(createCategory),
  averageRating: '4.5',
  ...attributes
});

export { createMovie };
