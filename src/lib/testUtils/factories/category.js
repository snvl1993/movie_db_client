const createCategory = (id, attributes = {}) => ({
  id,
  name: `Category ${id}`,
  ...attributes
});

export { createCategory };
