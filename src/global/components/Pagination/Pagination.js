import React from 'react';
import classnames from 'classnames';

import { getButtons } from './helpers';

import styles from './styles.module.css';

const Pagination = (props) => {
  const { className, page, totalPages, onClick } = props;

  if (!page || !totalPages) return null;

  const buttons = getButtons(page, totalPages);

  const classes = classnames(className, styles.component);
  const classesButton = classnames(styles.item, 'ui-button ui-link');

  const renderButton = (data) => {
    const { value, label } = data;

    const current = +value === +page;
    if (!value || current) {
      return (
        <li className={styles.item} key={label}>
          {label}
        </li>
      );
    }

    const onClickHandler = () => onClick(value);

    return (
      <li key={label}>
        <button
          className={classesButton}
          type="button"
          onClick={onClickHandler}
        >
          {label}
        </button>
      </li>
    );
  };

  return <ul className={classes}>{buttons.map(renderButton)}</ul>;
};

export { Pagination };
