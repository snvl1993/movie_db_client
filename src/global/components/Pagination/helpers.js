import uniqBy from 'lodash/uniqBy';

const removeInvalidButtons = (buttons, totalPages) =>
  buttons.filter((data) => {
    const { value } = data;
    return value > 0 && value <= totalPages;
  });

const insertEllipsis = (buttons) =>
  buttons.reduce((acc, button, i) => {
    const { value } = button;
    const next = buttons[i + 1];
    const result = [...acc, button];
    if (next && value + 1 < next.value) {
      result.push({ value: null, label: '...' });
    }

    return result;
  }, []);

const getButtons = (page, totalPages) => {
  const candidates = [
    { value: page - 1, label: 'Prev' },
    { value: 1, label: '1' },
    { value: 2, label: '2' },
    { value: page - 2, label: (page - 2).toString() },
    { value: page - 1, label: (page - 1).toString() },
    { value: page, label: page.toString() },
    { value: +page + 1, label: (+page + 1).toString() },
    { value: +page + 2, label: (+page + 2).toString() },
    { value: totalPages - 1, label: (totalPages - 1).toString() },
    { value: totalPages, label: totalPages.toString() },
    { value: +page + 1, label: 'Next' }
  ];

  const buttons = uniqBy(removeInvalidButtons(candidates, totalPages), 'label');

  return insertEllipsis(buttons);
};

export { getButtons };
