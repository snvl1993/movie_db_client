import React from 'react';
import classnames from 'classnames';

import styles from './styles.module.css';

const Stars = (props) => {
  const { className, value, number, interactive, onChange } = props;

  const stars = Array.from(Array(number), (_, i) => i + 1);

  const classes = classnames(className, styles.component, {
    [styles.interactive]: interactive
  });

  const renderStar = (i) => {
    const full = i <= value;
    const hollow = i > Math.ceil(value);
    const partial = !full && !hollow;

    const onClick = () => {
      if (interactive) onChange(i);
    };

    const classesStar = classnames(styles.star, {
      [styles.partial]: partial,
      [styles.hollow]: hollow,
      [styles.full]: !hollow && !partial
    });

    return (
      <button
        className={classesStar}
        onClick={onClick}
        type="button"
        disabled={!interactive}
        key={i}
      />
    );
  };

  return <div className={classes}>{stars.map(renderStar)}</div>;
};

export { Stars };
