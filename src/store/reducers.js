import form from '@services/form/reducer';
import movies from '@services/movies/reducers';
import user from '@services/user/reducer';

export default { form, movies, user };
