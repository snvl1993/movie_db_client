import { createMemoryHistory } from 'history';

import createStore from '@store/createStore';

const history = createMemoryHistory();
const createMockedStore = (initialState = {}) =>
  createStore(initialState, history);

export { history, createMockedStore as default };
