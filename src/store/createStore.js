import createSagaMiddleware, { END } from 'redux-saga';
import { applyMiddleware, createStore } from 'redux';
import { routerMiddleware as createRouterMiddleware } from 'connected-react-router';

import composeEnhancers from '@store/composeEnhancers';
import createRootReducer from '@store/createRootReducer';
import rootSaga from '@store/rootSaga';

export default (initialState, history) => {
  const routerMiddleware = createRouterMiddleware(history);
  const sagaMiddleware = createSagaMiddleware();
  const middlewares = [routerMiddleware, sagaMiddleware];
  const enhancer = composeEnhancers(applyMiddleware(...middlewares));
  const rootReducer = createRootReducer(history);

  const store = createStore(rootReducer, initialState, enhancer);

  store.runSaga = sagaMiddleware.run.bind(sagaMiddleware, rootSaga);
  store.close = () => {
    store.dispatch(END);
  };

  return store;
};
