import { compose } from 'redux';

import config from '@config';

const composeEnhancers = () => {
  if (config.environment === 'development') {
    return typeof window === 'object' &&
      window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
      ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
      : compose;
  }

  return compose;
};

export default composeEnhancers();
