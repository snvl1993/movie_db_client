import { all } from 'redux-saga/effects';

import { apiSaga } from '@api/saga';
import { authSaga } from '@services/auth/saga';
import { moviesSaga } from '@services/movies/saga';

export default function* rootSaga() {
  yield all([apiSaga(), authSaga(), moviesSaga()]);
}
