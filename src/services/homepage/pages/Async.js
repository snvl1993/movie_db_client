import loadable from '@loadable/component';

export const PageHome = loadable(() =>
  import(/* webpackChunkName: "page-home" */ './Homepage/Container')
);
