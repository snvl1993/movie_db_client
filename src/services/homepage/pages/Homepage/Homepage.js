import React from 'react';
import classnames from 'classnames';
import { Helmet } from 'react-helmet';
import { useLocation } from 'react-router';
import { parseQueryString } from '@lib/urlTools';
import { Link } from 'react-router-dom';

import { Container as Filter } from '@services/movies/components/Filter/Container';
import { Container as MovieList } from '@services/movies/components/MovieList/Container';

import styles from './styles.module.css';

const Homepage = (props) => {
  const { canCreateMovie } = props;

  const { search } = useLocation();

  const params = parseQueryString(search);
  const page = params.page || 1;

  const classes = classnames('ui-container', styles.component);
  const classesFilter = classnames('ui-section', styles.filter);

  const renderButton = () => {
    if (!canCreateMovie) return null;

    return (
      <Link className="ui-button ui-primary" to="/movies/new">
        Create Movie
      </Link>
    );
  };

  return (
    <div className={classes}>
      <Helmet>
        <title>Movies</title>
      </Helmet>

      <div className={styles.header}>
        <h1 className="ui-h1">Movies</h1>
        {renderButton()}
      </div>

      <div className={styles.content}>
        <Filter className={classesFilter} params={params} />
        <MovieList className={styles.movies} params={params} page={page} />
      </div>
    </div>
  );
};

export { Homepage, Homepage as default };
