import React from 'react';
import { useSelector } from 'react-redux';

import { Homepage } from './Homepage';
import { selectLoggedIn } from '@services/user/selectors';

const Container = (props) => {
  const isLoggedIn = useSelector(selectLoggedIn);

  return <Homepage {...props} canCreateMovie={isLoggedIn} />;
};

export { Container, Container as default };
