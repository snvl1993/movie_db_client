import React from 'react';
import { MemoryRouter } from 'react-router';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';

import createMockedStore from '@store/createMockedStore';
import { createMovie } from '@lib/testUtils/factories/movie';
import { Homepage } from '../Homepage';

describe('Homepage', () => {
  let props;
  let state;

  const movie1 = createMovie(1);
  const movie2 = createMovie(2);
  const movies = [movie1, movie2];

  const buildComponent = (props = {}, state = {}) => {
    const store = createMockedStore(state);

    return (
      <Provider store={store}>
        <MemoryRouter>
          <Homepage {...props} />
        </MemoryRouter>
      </Provider>
    );
  };

  beforeEach(() => {
    props = {};
    state = {
      movies: {
        index: {
          movies,
          meta: { totalCount: 5 },
          facets: {
            counts: { categories: [], ratings: [] },
            options: { categories: [] }
          }
        }
      }
    };
  });

  it('renders movies', () => {
    render(buildComponent(props, state));

    movies.forEach((movie) => {
      const { title } = movie;
      expect(screen.getByText(title)).toBeInTheDocument();
    });
  });

  it('renders pagination', () => {
    props = { ...props, page: 3 };

    render(buildComponent(props, state));

    expect(screen.getByText('1')).toBeInTheDocument();
    expect(screen.getByText('5')).toBeInTheDocument();
    expect(screen.getByText('Next')).toBeInTheDocument();
  });

  it('renders filter', () => {
    render(buildComponent(props, state));

    expect(screen.getByPlaceholderText('Search')).toBeInTheDocument();
  });
});
