import React from 'react';
import classnames from 'classnames';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';

import { Container as FormSignIn } from '../../components/FormSignIn/Container';

import styles from './styles.module.css';

const SignIn = () => {
  const classesForm = classnames('ui-section', styles.form);

  return (
    <div className={styles.component}>
      <Helmet>
        <title>Sign In</title>
      </Helmet>

      <div className={classesForm}>
        <h1 className="ui-h1 ui-margin">Sign In</h1>
        <FormSignIn />

        <Link className="ui-button ui-link" to="/auth/sign-up">
          Sign Up
        </Link>
      </div>
    </div>
  );
};

export { SignIn, SignIn as default };
