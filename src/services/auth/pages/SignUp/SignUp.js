import React from 'react';
import classnames from 'classnames';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';

import { Container as FormSignUp } from '../../components/FormSignUp/Container';

import styles from './styles.module.css';

const SignUp = () => {
  const classesForm = classnames('ui-section', styles.form);

  return (
    <div className={styles.component}>
      <Helmet>
        <title>Sign Up</title>
      </Helmet>

      <div className={classesForm}>
        <h1 className="ui-h1 ui-margin">Sign Up</h1>
        <FormSignUp />

        <Link className="ui-button ui-link" to="/auth/sign-in">
          Sign In
        </Link>
      </div>
    </div>
  );
};

export { SignUp, SignUp as default };
