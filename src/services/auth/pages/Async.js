import loadable from '@loadable/component';

export const PageSignIn = loadable(() =>
  import(/* webpackChunkName: "page-sign-in" */ './SignIn')
);

export const PageSignUp = loadable(() =>
  import(/* webpackChunkName: "page-sign-up" */ './SignUp')
);
