import { takeEvery, put } from 'redux-saga/effects';
import { push } from 'connected-react-router';

import { setFormError } from '@form/saga';
import { API_SIGN_UP, API_SIGN_IN } from '@api/constants/auth';
import { FORM_SIGN_IN, FORM_SIGN_UP } from './constants';

const errorActions = [API_SIGN_UP.fail, API_SIGN_IN.fail];
const successActions = [API_SIGN_UP.success, API_SIGN_IN.success];

function* setErrors(action) {
  const { type, payload } = action;
  const { json = {} } = payload;
  const { errors = {} } = json;

  const formNames = {
    [API_SIGN_UP.fail]: FORM_SIGN_UP,
    [API_SIGN_IN.fail]: FORM_SIGN_IN
  };

  yield setFormError(formNames[type], errors);
}
function* successSaga() {
  yield put(push('/'));
}

function* authSaga() {
  yield takeEvery(errorActions, setErrors);
  yield takeEvery(successActions, successSaga);
}

export { authSaga };
