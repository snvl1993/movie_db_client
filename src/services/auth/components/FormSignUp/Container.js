import React from 'react';
import { useDispatch } from 'react-redux';

import { FormSignUp } from './FormSignUp';
import { apiSignUp } from '@api/actions/auth';
import { useForm } from '@form/hooks';
import { validations } from './validations';
import { FORM_SIGN_UP } from '@services/auth/constants';

const Container = (props) => {
  const dispatch = useDispatch();

  const onSubmit = (values) => dispatch(apiSignUp(values));

  const data = useForm(FORM_SIGN_UP, { validations, onSubmit });
  const { formError, errors, values, setValue, submit, submitted } = data;

  const actions = {
    setValue,
    onSubmit: submit
  };

  return (
    <FormSignUp
      {...props}
      {...actions}
      formError={formError}
      errors={submitted ? errors : {}}
      values={values}
    />
  );
};

export { Container };
