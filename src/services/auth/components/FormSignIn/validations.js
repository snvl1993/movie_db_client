import { validateEmail, validatePresence } from '@form/utils/validators';

export const validations = {
  email: [validatePresence, validateEmail],
  password: [validatePresence]
};
