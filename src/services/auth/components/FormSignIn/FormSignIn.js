import React from 'react';

import { FormInput } from '@form/components/FormInput';
import { FormErrors } from '@form/components/FormErrors';

const FormSignIn = (props) => {
  const { className, formError, errors, values, setValue, onSubmit } = props;

  return (
    <form className={className} onSubmit={onSubmit}>
      <FormInput
        className="ui-field"
        name="email"
        label="Email"
        value={values.email}
        error={errors.email}
        onChange={setValue('email')}
      />

      <FormInput
        className="ui-field"
        name="password"
        label="Password"
        type="password"
        value={values.password}
        error={errors.password}
        onChange={setValue('password')}
      />

      <FormErrors errors={formError} />

      <div className="ui-buttons">
        <button className="ui-button ui-primary" type="submit">
          Sign In
        </button>
      </div>
    </form>
  );
};

export { FormSignIn };
