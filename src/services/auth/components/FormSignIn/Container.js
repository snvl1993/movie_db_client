import React from 'react';
import { useDispatch } from 'react-redux';

import { FormSignIn } from './FormSignIn';
import { apiSignIn } from '@api/actions/auth';
import { useForm } from '@form/hooks';
import { validations } from './validations';
import { FORM_SIGN_IN } from '@services/auth/constants';

const Container = (props) => {
  const dispatch = useDispatch();

  const onSubmit = (values) => dispatch(apiSignIn(values));

  const data = useForm(FORM_SIGN_IN, { validations, onSubmit });
  const { formError, errors, values, setValue, submit, submitted } = data;

  const actions = {
    setValue,
    onSubmit: submit
  };

  return (
    <FormSignIn
      {...props}
      {...actions}
      formError={formError}
      errors={submitted ? errors : {}}
      values={values}
    />
  );
};

export { Container };
