// Cookies
export const COOKIE_CSRF_TOKEN = 'CSRF-TOKEN';

// Headers
export const HEADER_CSRF_TOKEN = 'X-CSRF-Token';
