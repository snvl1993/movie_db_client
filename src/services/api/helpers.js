import camelCase from 'lodash/camelCase';
import snakeCase from 'lodash/snakeCase';
import isArray from 'lodash/isArray';
import isObject from 'lodash/isObject';
import reduce from 'lodash/reduce';
import merge from 'lodash/merge';

const formatPayload = (formatter) => (payload) => {
  if (payload instanceof File) return payload;
  if (isArray(payload)) return payload.map(formatPayload(formatter));
  if (!isObject(payload)) return payload;

  return reduce(
    payload,
    (result, value, key) =>
      merge(result, { [formatter(key)]: formatPayload(formatter)(value) }),
    {}
  );
};

const formatResponse = formatPayload(camelCase);
const formatRequest = formatPayload(snakeCase);

const getBody = (params) => JSON.stringify(params);

const DEFAULT_HEADERS = {
  Accept: 'application/json',
  'Content-Type': 'application/json'
};

const getHeaders = (headers = {}) => ({
  ...DEFAULT_HEADERS,
  ...headers
});

const sendRequest = ({
  method,
  url,
  params = {},
  headers = {},
  mode = 'same-origin',
}) => {
  const options = {
    mode,
    method,
    headers: getHeaders(headers),
    cache: 'no-store',
    credentials: 'include'
  };

  const body = getBody(params);
  if (method !== 'get' && body) {
    options.body = body;
  }

  return fetch(url, options);
};

const callApi = (route, params = {}, headers = {}) => {
  const { path, method } = route;
  const url = typeof path === 'function' ? path(params) : path;
  return sendRequest({ url, method, params, headers });
};

const createConstants = (base) => ({
  request: base,
  success: `${base}_SUCCEEDED`,
  fail: `${base}_FAILED`
});

const createAction = (type) => (payload = {}, params = {}) => {
  const { headers = {} } = params;
  return { type, payload, headers };
};

export {
  sendRequest,
  callApi,
  createAction,
  createConstants,
  formatPayload,
  formatResponse,
  formatRequest
};
