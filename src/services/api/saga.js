import Cookie from 'js-cookie';
import { takeEvery, call, put } from 'redux-saga/effects';

import { routes } from './routes';
import {
  callApi,
  createConstants,
  formatResponse,
  formatRequest
} from './helpers';
import { COOKIE_CSRF_TOKEN, HEADER_CSRF_TOKEN } from './constants';

const apiActions = Object.keys(routes);

function* getCsrf() {
  return yield call([Cookie, Cookie.get], COOKIE_CSRF_TOKEN);
}

function* makeRequest(action) {
  const { type, payload = {}, headers = {} } = action;
  const route = routes[type];

  const resultActions = createConstants(type);
  try {
    const csrfToken = yield getCsrf();
    const requestPayload = formatRequest(payload);
    const requestHeaders = { ...headers, [HEADER_CSRF_TOKEN]: csrfToken };
    const response = yield call(callApi, route, requestPayload, requestHeaders);

    // Get json from response and format it to camelCase
    const rawJson = yield call([response, response.json]);
    const json = formatResponse(rawJson);

    if (!response.ok || json.status === 'error' || json.success === false) {
      yield put({ type: resultActions.fail, payload: { json, response } });
      return;
    }

    yield put({
      type: resultActions.success,
      payload: { json, headers: response.headers, response }
    });
  } catch (error) {
    yield put({ type: resultActions.fail, payload: { error } });
  }
}

function* apiSaga() {
  yield takeEvery(apiActions, makeRequest);
}

export { apiSaga };
