import { createConstants } from '@api/helpers';

export const API_SIGN_UP = createConstants('API_SIGN_UP');
export const API_SIGN_IN = createConstants('API_SIGN_IN');
export const API_SIGN_OUT = createConstants('API_SIGN_OUT');
