import { createConstants } from '@api/helpers';

export const API_CATEGORIES_FETCH = createConstants('API_CATEGORIES_FETCH');
