import { createConstants } from '@api/helpers';

export const API_FACETS_FETCH = createConstants('API_FACETS_FETCH');
