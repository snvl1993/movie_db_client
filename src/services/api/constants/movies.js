import { createConstants } from '@api/helpers';

export const API_MOVIES_FETCH = createConstants('API_MOVIES_FETCH');
export const API_MOVIE_FETCH = createConstants('API_MOVIE_FETCH');
export const API_MOVIE_CREATE = createConstants('API_MOVIE_CREATE');
export const API_MOVIE_UPDATE = createConstants('API_MOVIE_UPDATE');
export const API_MOVIE_DESTROY = createConstants('API_MOVIE_DESTROY');
export const API_MOVIE_RATE = createConstants('API_MOVIE_RATE');
