import { createAction } from '@api/helpers';
import { API_USER_FETCH } from '@api/constants/user';

const apiUserFetch = createAction(API_USER_FETCH.request);

export { apiUserFetch };
