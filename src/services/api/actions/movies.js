import { createAction } from '@api/helpers';
import {
  API_MOVIES_FETCH,
  API_MOVIE_FETCH,
  API_MOVIE_CREATE,
  API_MOVIE_UPDATE,
  API_MOVIE_RATE,
  API_MOVIE_DESTROY
} from '@api/constants/movies';

const apiMoviesFetch = createAction(API_MOVIES_FETCH.request);
const apiMovieFetch = createAction(API_MOVIE_FETCH.request);
const apiMovieCreate = createAction(API_MOVIE_CREATE.request);
const apiMovieUpdate = createAction(API_MOVIE_UPDATE.request);
const apiMovieDestroy = createAction(API_MOVIE_DESTROY.request);
const apiMovieRate = createAction(API_MOVIE_RATE.request);

export {
  apiMoviesFetch,
  apiMovieFetch,
  apiMovieCreate,
  apiMovieUpdate,
  apiMovieDestroy,
  apiMovieRate
};
