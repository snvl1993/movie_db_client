import { createAction } from '@api/helpers';
import { API_SIGN_IN, API_SIGN_OUT, API_SIGN_UP } from '@api/constants/auth';

const apiSignIn = createAction(API_SIGN_IN.request);
const apiSignUp = createAction(API_SIGN_UP.request);
const apiSignOut = createAction(API_SIGN_OUT.request);

export { apiSignIn, apiSignUp, apiSignOut };
