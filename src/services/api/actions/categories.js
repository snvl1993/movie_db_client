import { createAction } from '@api/helpers';
import { API_CATEGORIES_FETCH } from '@api/constants/categories';

const apiCategoriesFetch = createAction(API_CATEGORIES_FETCH.request);

export { apiCategoriesFetch };
