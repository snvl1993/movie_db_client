import { createAction } from '@api/helpers';
import { API_FACETS_FETCH } from '@api/constants/facets';

const apiFacetsFetch = createAction(API_FACETS_FETCH.request);

export { apiFacetsFetch };
