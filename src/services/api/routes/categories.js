import { API_CATEGORIES_FETCH } from '@api/constants/categories';

export default {
  [API_CATEGORIES_FETCH.request]: {
    path: '/api/v1/categories',
    method: 'get'
  }
};
