import { API_SIGN_UP, API_SIGN_IN, API_SIGN_OUT } from '@api/constants/auth';

export default {
  [API_SIGN_UP.request]: {
    path: '/api/v1/auth/sign_up',
    method: 'post'
  },

  [API_SIGN_IN.request]: {
    path: '/api/v1/auth/sign_in',
    method: 'post'
  },

  [API_SIGN_OUT.request]: {
    path: '/api/v1/auth/sign_out',
    method: 'delete'
  }
};
