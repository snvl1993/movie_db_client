import { buildQueryString } from '@lib/urlTools';
import {
  API_MOVIES_FETCH,
  API_MOVIE_FETCH,
  API_MOVIE_CREATE,
  API_MOVIE_UPDATE,
  API_MOVIE_RATE, API_MOVIE_DESTROY
} from '@api/constants/movies';

export default {
  [API_MOVIES_FETCH.request]: {
    path: (params) => `/api/v1/movies${buildQueryString(params)}`,
    method: 'get'
  },

  [API_MOVIE_FETCH.request]: {
    path: (params) => `/api/v1/movies/${params.id}`,
    method: 'get'
  },

  [API_MOVIE_CREATE.request]: {
    path: '/api/v1/movies',
    method: 'post'
  },

  [API_MOVIE_UPDATE.request]: {
    path: (params) => `/api/v1/movies/${params.id}`,
    method: 'put'
  },

  [API_MOVIE_DESTROY.request]: {
    path: (params) => `/api/v1/movies/${params.id}`,
    method: 'delete'
  },

  [API_MOVIE_RATE.request]: {
    path: (params) => `/api/v1/movies/${params.id}/rate`,
    method: 'post'
  }
};
