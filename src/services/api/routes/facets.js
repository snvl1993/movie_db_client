import { API_FACETS_FETCH } from '@api/constants/facets';
import { buildQueryString } from '@lib/urlTools';

export default {
  [API_FACETS_FETCH.request]: {
    path: (params) => `/api/v1/facets${buildQueryString(params)}`,
    method: 'get'
  }
};
