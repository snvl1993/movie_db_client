import { API_USER_FETCH } from '@api/constants/user';

export default {
  [API_USER_FETCH.request]: {
    path: '/api/v1/user',
    method: 'get'
  }
};
