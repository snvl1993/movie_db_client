import auth from './auth';
import movies from './movies';
import user from './user';
import facets from './facets';
import categories from './categories';

const routes = {
  ...auth,
  ...movies,
  ...user,
  ...facets,
  ...categories
};

export { routes };
