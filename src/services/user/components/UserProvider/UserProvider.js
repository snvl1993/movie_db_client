import React from 'react';
import { useDispatch } from 'react-redux';

import { apiUserFetch } from '@api/actions/user';

const UserProvider = (props) => {
  const { children } = props;

  const dispatch = useDispatch();

  const onMount = () => {
    dispatch(apiUserFetch());
  };

  React.useEffect(onMount, [dispatch]);

  return children;
};

export { UserProvider };
