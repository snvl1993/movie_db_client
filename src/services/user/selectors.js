import get from 'lodash/get';
import { createSelector } from 'reselect';

const selectUser = (state) => get(state, 'user', {});
const selectLoggedIn = createSelector(selectUser, (user) => !!user?.id);

export { selectUser, selectLoggedIn };
