import { API_USER_FETCH } from '@api/constants/user';
import { API_SIGN_IN, API_SIGN_UP, API_SIGN_OUT } from '@api/constants/auth';

const initialState = null;

const reducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case API_SIGN_IN.success:
    case API_SIGN_UP.success:
    case API_USER_FETCH.success: {
      const { user } = payload.json;
      return user;
    }

    case API_SIGN_OUT.success: {
      return initialState;
    }

    default:
      return state;
  }
};

export default reducer;
