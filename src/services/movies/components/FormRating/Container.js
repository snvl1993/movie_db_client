import React from 'react';
import { useDispatch } from 'react-redux';

import { FormRating } from './FormRating';
import { apiMovieRate } from '@api/actions/movies';

const Container = (props) => {
  const { movieId } = props;

  const dispatch = useDispatch();

  const onChange = (rate) => {
    dispatch(apiMovieRate({ id: movieId, rate }));
  };

  return <FormRating {...props} onChange={onChange} />;
};

export { Container };
