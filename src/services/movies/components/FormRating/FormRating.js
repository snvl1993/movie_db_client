import React from 'react';
import classnames from 'classnames';

import { Stars } from '@global/components/Stars';

import styles from './styles.module.css';

const FormRating = (props) => {
  const { className, rate, onChange } = props;

  const classes = classnames(className, styles.component);

  const onChangeHandler = (rate) => {
    onChange(rate)
  };

  return (
    <div className={classes}>
      <div className={styles.label}>Rate this movie!</div>

      <Stars
        className={styles.stars}
        value={rate}
        number={5}
        interactive
        onChange={onChangeHandler}
      />
    </div>
  );
};

export { FormRating };
