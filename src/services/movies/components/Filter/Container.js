import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { push } from 'connected-react-router';

import { Filter } from './Filter';
import { getRatingsFacets, getCategoriesFacets, getDefaults } from './helper';
import { apiFacetsFetch } from '@api/actions/facets';
import { selectFacets } from '@services/movies/selectors';
import { useForm } from '@form/hooks';
import { buildQueryString } from '@lib/urlTools';
import { FORM_FILTER } from '@services/movies/constants';

const Container = (props) => {
  const { params } = props;

  const dispatch = useDispatch();
  const { options, counts } = useSelector(selectFacets);

  const onSubmit = (values) => dispatch(push(buildQueryString(values)));
  const defaults = getDefaults(params);

  const { values, setValue, resetValues, submit } = useForm(FORM_FILTER, {
    defaults,
    onSubmit
  });

  const onMount = () => {
    dispatch(apiFacetsFetch(params));
    resetValues(defaults);
  };

  const onChange = () => {
    dispatch(apiFacetsFetch(values));
  };

  React.useEffect(onMount, [dispatch, params]);
  React.useEffect(onChange, [dispatch, values.ratings, values.categories]);

  if (!options || !counts) return null;

  const facetsCategories = getCategoriesFacets(options, counts);
  const facetsRatings = getRatingsFacets(counts);

  return (
    <Filter
      {...props}
      categories={facetsCategories}
      ratings={facetsRatings}
      values={values}
      setValue={setValue}
      submit={submit}
    />
  );
};

export { Container };
