import React from 'react';
import classnames from 'classnames';

import { Facets } from '@services/movies/components/Facets';
import { FormInput } from '@form/components/FormInput';

import styles from './styles.module.css';

const Filter = (props) => {
  const { className, categories, ratings, values, setValue, submit } = props;

  const classesSearch = classnames('ui-field', styles.section);
  const classesButton = classnames('ui-button ui-primary', styles.button);

  return (
    <div className={className}>
      <form className={styles.section} onSubmit={submit}>
        <FormInput
          className={classesSearch}
          name="search"
          placeholder="Search"
          value={values.search}
          onChange={setValue('search')}
        />

        <Facets
          className={styles.section}
          options={ratings}
          selected={values.ratings}
          onChange={setValue('ratings')}
        />

        <Facets
          className={styles.section}
          options={categories}
          selected={values.categories}
          onChange={setValue('categories')}
        />

        <button className={classesButton} type="submit">
          Search
        </button>
      </form>
    </div>
  );
};

export { Filter };
