const getCategoriesFacets = (options, counts) => {
  const hash = counts.categories.reduce((acc, data) => {
    const { value, count } = data;
    return { ...acc, [value]: count };
  }, {});

  return options.categories.map((option) => {
    const { value } = option;
    return { ...option, count: hash[value] || 0 };
  });
};

const getRatingsFacets = (counts) => {
  return counts.ratings.map((data) => {
    const { value } = data;
    const label = value === 1 ? '1 Star' : `${value} Stars`;
    return { ...data, label };
  });
};

const getDefaults = (params) => ({
  search: params.search || '',
  ratings: (params.ratings || []).map((v) => parseInt(v)),
  categories: (params.categories || []).map((v) => parseInt(v))
})

export { getCategoriesFacets, getRatingsFacets, getDefaults };
