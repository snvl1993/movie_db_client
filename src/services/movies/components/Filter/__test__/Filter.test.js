import React from 'react';
import { MemoryRouter } from 'react-router';
import { fireEvent, render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';

import createMockedStore from '@store/createMockedStore';
import { Filter } from '../Filter';

describe('Filter', () => {
  let props;

  const submit = jest.fn();
  const setValue = jest.fn();

  const categories = [
    { value: 1, label: 'Category 1', count: 5 },
    { value: 2, label: 'Category 2', count: 15 },
    { value: 3, label: 'Category 3', count: 1 }
  ];

  const ratings = [
    { value: 1, label: '1 Star', count: 0 },
    { value: 2, label: '2 Stars', count: 5 },
    { value: 3, label: '3 Stars', count: 2 }
  ];

  const buildComponent = (props = {}) => {
    const store = createMockedStore();

    return (
      <Provider store={store}>
        <MemoryRouter>
          <Filter {...props} />
        </MemoryRouter>
      </Provider>
    );
  };

  beforeEach(() => {
    props = {
      categories,
      ratings,
      setValue,
      submit,
      values: { categories: [], ratings: [] }
    };
  });

  afterEach(() => {
    submit.mockReset();
    setValue.mockReset();
  });

  it('renders', () => {
    render(buildComponent(props));

    const fieldSearch = screen.getByPlaceholderText('Search');

    // Renders fields
    expect(fieldSearch).toBeInTheDocument();

    // Renders categories facets
    categories.forEach((category) => {
      const { label, count } = category;
      const text = `${label} (${count})`;
      expect(screen.getByLabelText(text)).toBeInTheDocument();
    });

    // Renders ratings facets
    ratings.forEach((category) => {
      const { label, count } = category;
      const text = `${label} (${count})`;
      expect(screen.getByLabelText(text)).toBeInTheDocument();
    });

    // Submits with empty values
    fireEvent.click(screen.getByText('Search'));
    expect(submit).toBeCalledTimes(1);
  });
});
