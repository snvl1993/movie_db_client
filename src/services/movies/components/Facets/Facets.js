import React from 'react';
import classnames from 'classnames';

import styles from './styles.module.css';

const Facets = (props) => {
  const { className, options, selected, onChange } = props;

  const renderFacet = (option) => {
    const { value, label, count } = option;
    const disabled = count === 0;
    const checked = selected.includes(value);

    const classesFacet = classnames(styles.facet, { [styles.disabled]: disabled });

    const onChangeHandler = () => {
      const newValues = checked
        ? selected.filter((v) => v !== value)
        : [...selected, value];

      onChange(newValues);
    };

    return (
      <li className={classesFacet} key={value}>
        <label>
          <input
            type="checkbox"
            checked={checked}
            onChange={onChangeHandler}
            disabled={disabled}
          />
          {label} ({count})
        </label>
      </li>
    );
  };

  return (
    <div className={className}>
      <ul>{options.map(renderFacet)}</ul>
    </div>
  );
};

export { Facets };
