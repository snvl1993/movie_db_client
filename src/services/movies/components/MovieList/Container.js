import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { push } from 'connected-react-router';

import { MovieList } from './MovieList';
import { apiMoviesFetch } from '@api/actions/movies';
import { selectMovies, selectMoviesMeta } from '@services/movies/selectors';
import { buildQueryString } from '@lib/urlTools';

const Container = (props) => {
  const { params, page } = props;
  const { categories, ratings, search } = params;

  const dispatch = useDispatch();

  const movies = useSelector(selectMovies);
  const { totalCount } = useSelector(selectMoviesMeta);

  const onClickPagination = (newPage) => {
    const url = buildQueryString({ ...params, page: newPage });
    dispatch(push(url));
  };

  React.useEffect(() => {
    dispatch(apiMoviesFetch({ search, categories, ratings, page }));
  }, [dispatch, categories, ratings, search, page]);

  return (
    <MovieList
      {...props}
      movies={movies}
      page={page}
      totalPages={totalCount}
      onClickPagination={onClickPagination}
    />
  );
};

export { Container };
