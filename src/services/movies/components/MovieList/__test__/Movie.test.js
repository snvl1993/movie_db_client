import React from 'react';
import { MemoryRouter } from 'react-router';
import { fireEvent, render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';

import createMockedStore from '@store/createMockedStore';
import { createMovie } from '@lib/testUtils/factories/movie';
import { MovieList } from '../MovieList';

describe('MovieList', () => {
  let props;

  const onClickPagination = jest.fn();

  const movie1 = createMovie(1);
  const movie2 = createMovie(2);
  const movies = [movie1, movie2];

  const buildComponent = (props = {}) => {
    const store = createMockedStore();

    return (
      <Provider store={store}>
        <MemoryRouter>
          <MovieList {...props} />
        </MemoryRouter>
      </Provider>
    );
  };

  beforeEach(() => {
    props = { onClickPagination, movies, page: 2, totalPages: 4 };
  });

  afterEach(() => {
    onClickPagination.mockReset();
  });

  it('renders movies and pagination', () => {
    render(buildComponent(props));

    // renders movies
    movies.forEach((movie) => {
      const { title } = movie;
      expect(screen.getByText(title)).toBeInTheDocument();
    });

    // renders pagination
    expect(screen.getByText('1')).toBeInTheDocument();
    expect(screen.getByText('2')).toBeInTheDocument();
    expect(screen.getByText('Next')).toBeInTheDocument();
    expect(screen.getByText('Prev')).toBeInTheDocument();

    fireEvent.click(screen.getByText('1'));
    expect(onClickPagination).toBeCalledTimes(1);
    expect(onClickPagination).toBeCalledWith(1);
  });
});
