import React from 'react';
import classnames from 'classnames';

import { Container as Movie } from '@services/movies/components/Movie/Container';
import { Pagination } from '@global/components/Pagination';

import styles from './styles.module.css';

const MovieList = (props) => {
  const { className, movies, page, totalPages, onClickPagination } = props;

  const isEmpty = !movies.length

  const classes = classnames(className, styles.component);
  const classesMovie = classnames(styles.movie, 'ui-section');

  const renderMovie = (movie) => {
    return <Movie className={classesMovie} data={movie} key={movie.id} />;
  };

  return (
    <div className={classes}>
      {isEmpty && <div className="ui-section">Sorry, nothing was found</div> }
      {movies.map(renderMovie)}
      <Pagination page={page} totalPages={totalPages} onClick={onClickPagination} />
    </div>
  );
};

export { MovieList };
