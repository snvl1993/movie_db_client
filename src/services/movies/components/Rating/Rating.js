import React from 'react';
import classnames from 'classnames';

import { Stars } from '@global/components/Stars';

import styles from './styles.module.css';

const Rating = (props) => {
  const { className, rating } = props;

  if (!rating) return null;

  const formattedRating = Number(rating).toFixed(1)

  const classes = classnames(className, styles.component);

  return (
    <div className={classes}>
      <Stars className={styles.stars} value={rating} number={5} />
      <div className={styles.label}>{formattedRating}</div>
    </div>
  );
};

export { Rating };
