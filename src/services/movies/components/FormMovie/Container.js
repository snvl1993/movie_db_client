import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { FormMovie } from './FormMovie';
import {
  selectCategories,
  selectMoviesFormLoading
} from '@services/movies/selectors';
import { apiMovieCreate, apiMovieUpdate } from '@api/actions/movies';
import { apiCategoriesFetch } from '@api/actions/categories';

const Container = (props) => {
  const { movie } = props;

  const id = movie?.id;
  const dispatch = useDispatch();
  const loading = useSelector(selectMoviesFormLoading);
  const recordsCategories = useSelector(selectCategories);

  const onMount = () => {
    dispatch(apiCategoriesFetch());
  };

  const onSubmit = (values) => {
    const payload = { ...movie, ...values, id };
    const action = id ? apiMovieUpdate : apiMovieCreate;
    dispatch(action(payload));
  };

  React.useEffect(onMount, [dispatch]);

  return (
    <FormMovie
      {...props}
      onSubmit={onSubmit}
      loading={loading}
      recordsCategories={recordsCategories}
    />
  );
};

export { Container };
