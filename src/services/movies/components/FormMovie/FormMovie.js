import React from 'react';
import classnames from 'classnames';

import { useForm } from '@form/hooks';
import { FormInput } from '@form/components/FormInput';
import { FormSelect } from '@form/components/FormSelect';
import { FormErrors } from '@form/components/FormErrors';
import { FORM_MOVIE } from '@services/movies/constants';

import styles from './styles.module.css';

const FormMovie = (props) => {
  const { className, movie, onSubmit, loading, recordsCategories } = props;
  const { title, text, categories } = movie;

  const optionsCategories = recordsCategories.map((category) => {
    const { id, name } = category;
    return { value: id, label: name };
  });

  const defaults = {
    title,
    text,
    categories: categories?.map((c) => c.id) ?? []
  };

  const { errors, formError, values, setValue, submit } = useForm(FORM_MOVIE, {
    defaults,
    onSubmit
  });

  const classes = classnames(styles.component, className);

  return (
    <form className={classes} onSubmit={submit}>
      <FormInput
        className="ui-field"
        name="title"
        label="Title"
        value={values.title}
        error={errors.title}
        onChange={setValue('title')}
      />

      <FormSelect
        className="ui-field"
        name="categories"
        label="Categories"
        value={values.categories}
        error={errors.categories}
        onChange={setValue('categories')}
        options={optionsCategories}
        multiple
      />

      <FormInput
        className="ui-field"
        name="text"
        label="Text"
        value={values.text}
        error={errors.text}
        onChange={setValue('text')}
        type="textarea"
      />

      <FormErrors errors={formError} />

      <button className="ui-button ui-primary" type="submit" disabled={loading}>
        Submit
      </button>
    </form>
  );
};

FormMovie.defaultProps = {
  movie: {}
};

export { FormMovie };
