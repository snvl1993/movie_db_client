import React from 'react';
import { MemoryRouter } from 'react-router';
import { fireEvent, render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';

import createMockedStore from '@store/createMockedStore';
import { createMovie } from '@lib/testUtils/factories/movie';
import { createCategory } from '@lib/testUtils/factories/category';
import { FormMovie } from '../FormMovie';

describe('FormMovie', () => {
  let props;

  const onSubmit = jest.fn();

  const movie = createMovie(1);
  const recordsCategories = [...movie.categories, createCategory(30)];

  const buildComponent = (props = {}) => {
    const store = createMockedStore();

    return (
      <Provider store={store}>
        <MemoryRouter>
          <FormMovie {...props} />
        </MemoryRouter>
      </Provider>
    );
  };

  beforeEach(() => {
    props = { onSubmit, recordsCategories, loading: false };
  });

  afterEach(() => {
    onSubmit.mockReset();
  });

  test('create', () => {
    render(buildComponent(props));

    const fieldTitle = screen.getByLabelText('Title');
    const fieldText = screen.getByLabelText('Text');
    const fieldCategories = screen.getByLabelText('Categories');

    // Renders fields
    expect(fieldTitle).toBeInTheDocument();
    expect(fieldText).toBeInTheDocument();
    expect(fieldCategories).toBeInTheDocument();

    // Renders category options
    recordsCategories.forEach((category) => {
      const { name } = category;
      expect(screen.getByText(name)).toBeInTheDocument();
    });

    // Submits with empty values
    fireEvent.click(screen.getByText('Submit'));
    expect(onSubmit).toBeCalledTimes(1);
    expect(onSubmit).toBeCalledWith({ categories: [] }, expect.anything());
    onSubmit.mockReset();

    // Submits with entered values
    fireEvent.change(fieldTitle, { target: { value: 'Movie' } });
    fireEvent.change(fieldText, { target: { value: 'Text' } });

    fireEvent.click(screen.getByText('Submit'));
    expect(onSubmit).toBeCalledTimes(1);
    expect(onSubmit).toBeCalledWith(
      { title: 'Movie', text: 'Text', categories: [] },
      expect.anything()
    );
  });

  test('edit', () => {
    render(buildComponent({ ...props, movie }));

    const fieldTitle = screen.getByLabelText('Title');
    const fieldText = screen.getByLabelText('Text');
    const fieldCategories = screen.getByLabelText('Categories');

    // Renders fields
    expect(fieldTitle).toBeInTheDocument();
    expect(fieldText).toBeInTheDocument();
    expect(fieldCategories).toBeInTheDocument();

    // Submits with movie values
    fireEvent.click(screen.getByText('Submit'));
    expect(onSubmit).toBeCalledTimes(1);
    expect(onSubmit).toBeCalledWith(
      { title: 'Movie 1', text: 'Movie 1 Text', categories: [1, 2, 3] },
      expect.anything()
    );
  });
});
