import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Movie } from './Movie';
import { apiMovieDestroy } from '@api/actions/movies';
import { selectUser, selectLoggedIn } from '@services/user/selectors';

const Container = (props) => {
  const { data } = props;
  const { id, userId } = data;

  const dispatch = useDispatch();
  const user = useSelector(selectUser);
  const isLoggedIn = useSelector(selectLoggedIn);

  const onDelete = () => {
    const confirmed = window.confirm('Are you sure?');

    if (!confirmed) return;

    dispatch(apiMovieDestroy({ id }));
  };

  const editable = isLoggedIn && user.id === userId;

  return (
    <Movie
      {...props}
      rateable={isLoggedIn}
      editable={editable}
      onDelete={onDelete}
    />
  );
};

export { Container };
