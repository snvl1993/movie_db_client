import React from 'react';
import { MemoryRouter } from 'react-router';
import { fireEvent, render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';

import createMockedStore from '@store/createMockedStore';
import { createMovie } from '@lib/testUtils/factories/movie';
import { Movie } from '../Movie';

describe('Movie', () => {
  let props;

  const onDelete = jest.fn();

  const movie = createMovie(1);

  const buildComponent = (props = {}) => {
    const store = createMockedStore();

    return (
      <Provider store={store}>
        <MemoryRouter>
          <Movie {...props} />
        </MemoryRouter>
      </Provider>
    );
  };

  beforeEach(() => {
    props = { onDelete, data: movie, rateable: false, editable: false };
  });

  afterEach(() => {
    onDelete.mockReset();
  });

  test('non editable movie', () => {
    render(buildComponent(props));

    // renders fields
    const { title, text, categories, averageRating } = movie;
    expect(screen.getByText(title)).toBeInTheDocument();
    expect(screen.getByText(text)).toBeInTheDocument();
    expect(screen.getByText(averageRating)).toBeInTheDocument();

    categories.forEach((category) => {
      expect(screen.getByText(category.name)).toBeInTheDocument();
    });

    // do not render action buttons
    expect(screen.queryByText('Edit')).not.toBeInTheDocument();
    expect(screen.queryByText('Delete')).not.toBeInTheDocument();
    expect(screen.queryByText('Rate this movie!')).not.toBeInTheDocument();
  });

  test('rateable movie', () => {
    render(buildComponent({ ...props, rateable: true }));

    // do not render action buttons
    expect(screen.queryByText('Edit')).not.toBeInTheDocument();
    expect(screen.queryByText('Delete')).not.toBeInTheDocument();

    // renders rate form
    expect(screen.getByText('Rate this movie!')).toBeInTheDocument();
  });

  test('editable movie', () => {
    render(buildComponent({ ...props, editable: true }));

    // do not render rate form
    expect(screen.queryByText('Rate this movie!')).not.toBeInTheDocument();

    // renders action buttons
    const actionEdit = screen.getByText('Edit');
    const actionDelete = screen.getByText('Delete');

    expect(actionEdit).toBeInTheDocument();
    expect(actionDelete).toBeInTheDocument();

    fireEvent.click(actionDelete);
    expect(onDelete).toBeCalledTimes(1);
  });
});
