import React from 'react';
import classnames from 'classnames';
import { Link } from 'react-router-dom';

import { Rating } from '@services/movies/components/Rating';
import { Container as FormRating } from '@services/movies/components/FormRating/Container';

import styles from './styles.module.css';

const Movie = (props) => {
  const { className, data, rateable, editable, onDelete } = props;
  const { id, title, text, categories, averageRating, rate } = data;

  const urlShow = `/movies/${id}`;
  const urlEdit = `/movies/${id}/edit`;

  const classes = classnames(className, styles.component);

  const renderCategory = (category) => {
    const { id, name } = category;

    return <li key={id}>{name}</li>;
  };

  const renderCategories = () => {
    if (!categories?.length) return null;

    return (
      <ul className={styles.categories}>{categories.map(renderCategory)}</ul>
    );
  };

  const renderFormRating = () => {
    if (!rateable) return null;

    return (
      <FormRating className={styles.formRating} movieId={id} rate={rate} />
    );
  };

  const renderActions = () => {
    if (!editable) return null;

    return (
      <div className={styles.actions}>
        <Link className="ui-link" to={urlEdit}>
          Edit
        </Link>

        <button className="ui-link" onClick={onDelete} type="button">
          Delete
        </button>
      </div>
    );
  };

  return (
    <div className={classes}>
      <div className={styles.header}>
        <Link to={urlShow} className="ui-link">
          <h3 className="ui-h3">{title}</h3>
        </Link>
        <Rating rating={averageRating} />
      </div>

      <div className={styles.subHeader}>
        {renderCategories()}
        {renderActions()}
      </div>

      <p className="ui-text">{text}</p>

      {renderFormRating()}
    </div>
  );
};

export { Movie };
