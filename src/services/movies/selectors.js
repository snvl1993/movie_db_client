import get from 'lodash/get';
import { createSelector } from 'reselect';

const selectMoviesState = (state) => get(state, 'movies', {});
const selectMoviesIndex = createSelector(selectMoviesState, (state) =>
  get(state, 'index', {})
);

const selectMoviesForm = createSelector(selectMoviesState, (state) =>
  get(state, 'form', {})
);

const selectMoviesMovie = createSelector(selectMoviesState, (state) =>
  get(state, 'movie', {})
);

const selectMovies = createSelector(selectMoviesIndex, (state) =>
  get(state, 'movies', [])
);

const selectMoviesMeta = createSelector(selectMoviesIndex, (state) =>
  get(state, 'meta', {})
);

const selectFacets = createSelector(selectMoviesIndex, (state) =>
  get(state, 'facets', {})
);

const selectMovie = createSelector(selectMoviesMovie, (state) =>
  get(state, 'movie', {})
);

const selectMoviesFormLoading = createSelector(selectMoviesForm, (state) =>
  get(state, 'loading', false)
);

const selectCategories = createSelector(selectMoviesForm, (state) =>
  get(state, 'categories', [])
);

export {
  selectMoviesState,
  selectMoviesIndex,
  selectMovies,
  selectMoviesMeta,
  selectFacets,
  selectMovie,
  selectMoviesFormLoading,
  selectCategories
};
