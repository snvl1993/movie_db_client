import React from 'react';
import { Helmet } from 'react-helmet';

import { Container as FormMovie } from '@services/movies/components/FormMovie/Container';

const Edit = (props) => {
  const { movie } = props;

  const { title } = movie;

  if (!movie?.id) return null;

  return (
    <div className="ui-container ui-section">
      <Helmet>
        <title>Edit Movie "{title}"</title>
      </Helmet>

      <h1 className="ui-h1 ui-margin">Edit Movie "{title}"</h1>
      <FormMovie movie={movie} />
    </div>
  );
};

export { Edit };
