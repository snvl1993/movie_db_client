import React from 'react';
import { Helmet } from 'react-helmet';

import { Container as FormMovie } from '@services/movies/components/FormMovie/Container';

const Create = () => (
  <div className="ui-container ui-section">
    <Helmet>
      <title>Create Movie</title>
    </Helmet>

    <h1 className="ui-h1 ui-margin">Create Movie</h1>
    <FormMovie />
  </div>
);

export { Create, Create as default };
