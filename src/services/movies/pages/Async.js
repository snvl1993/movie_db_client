import loadable from '@loadable/component';

export const PageMovieShow = loadable(() =>
  import(/* webpackChunkName: "page-movie-show" */ './Show/Container')
);

export const PageMovieEdit = loadable(() =>
  import(/* webpackChunkName: "page-movie-edit" */ './Edit/Container')
);

export const PageMovieCreate = loadable(() =>
  import(/* webpackChunkName: "page-movie-create" */ './Create')
);
