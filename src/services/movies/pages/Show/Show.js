import React from 'react';
import { Helmet } from 'react-helmet';

import { Container as Movie } from '@services/movies/components/Movie/Container';

const Show = (props) => {
  const { movie } = props;
  const { title } = movie;

  return (
    <div className="ui-container ui-section">
      <Helmet>
        <title>{title}</title>
      </Helmet>

      <Movie data={movie} />
    </div>
  );
};

export { Show };
