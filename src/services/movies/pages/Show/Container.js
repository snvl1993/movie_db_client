import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';

import { Show } from './Show';
import { selectMovie } from '@services/movies/selectors';
import { apiMovieFetch } from '@api/actions/movies';

const Container = (props) => {
  const { id } = useParams();

  const dispatch = useDispatch();
  const movie = useSelector(selectMovie);

  const onMount = () => {
    dispatch(apiMovieFetch({ id }));
  };

  React.useEffect(onMount, [dispatch, id]);

  return <Show {...props} movie={movie} />;
};

export { Container, Container as default };
