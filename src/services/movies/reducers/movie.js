import { API_MOVIE_FETCH, API_MOVIE_RATE } from '@api/constants/movies';

const initialState = {
  movie: {}
};

const reducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case API_MOVIE_RATE.success:
    case API_MOVIE_FETCH.success: {
      const { movie } = payload.json;
      return { ...state, movie };
    }

    default:
      return state;
  }
};

export default reducer;
