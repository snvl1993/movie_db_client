import { combineReducers } from 'redux';

import index from './movies';
import movie from './movie';
import form from './form';

export default combineReducers({ index, movie, form });
