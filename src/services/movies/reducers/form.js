import { API_MOVIE_CREATE, API_MOVIE_UPDATE } from '@api/constants/movies';
import { API_CATEGORIES_FETCH } from '@api/constants/categories';

const initialState = {
  loading: false,
  categories: []
};

const reducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case API_MOVIE_CREATE.request:
    case API_MOVIE_UPDATE.request:
      return { ...state, loading: true };

    case API_MOVIE_CREATE.fail:
    case API_MOVIE_CREATE.success:
    case API_MOVIE_UPDATE.fail:
    case API_MOVIE_UPDATE.success:
      return { ...state, loading: false };

    case API_CATEGORIES_FETCH.success: {
      const { categories } = payload.json;
      return { ...state, categories };
    }

    default:
      return state;
  }
};

export default reducer;
