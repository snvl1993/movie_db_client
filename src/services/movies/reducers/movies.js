import { API_MOVIES_FETCH, API_MOVIE_RATE } from '@api/constants/movies';
import { API_FACETS_FETCH } from '@api/constants/facets';

const initialState = {
  facets: {},
  movies: [],
  meta: {}
};

const reducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case API_MOVIES_FETCH.success: {
      const { movies, meta } = payload.json;
      return { ...state, movies, meta };
    }

    case API_FACETS_FETCH.success: {
      const { options, counts } = payload.json;
      return { ...state, facets: { options, counts } };
    }

    case API_MOVIE_RATE.success: {
      const { movie } = payload.json;
      const { id } = movie;
      const newMovies = state.movies.map((m) => (m.id === id ? movie : m));
      return { ...state, movies: newMovies };
    }

    default:
      return state;
  }
};

export default reducer;
