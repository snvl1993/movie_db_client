import { put, takeEvery } from 'redux-saga/effects';
import { push } from 'connected-react-router';

import { setFormError } from '@form/saga';
import { apiFacetsFetch } from '@api/actions/facets';
import {
  API_MOVIE_UPDATE,
  API_MOVIE_CREATE,
  API_MOVIE_RATE,
  API_MOVIE_DESTROY,
  API_MOVIE_FETCH
} from '@api/constants/movies';
import { FORM_MOVIE } from '@services/movies/constants';

const redirectErrorActions = [API_MOVIE_FETCH.fail, API_MOVIE_DESTROY.success];
const validationErrorActions = [API_MOVIE_UPDATE.fail, API_MOVIE_CREATE.fail];
const successActions = [API_MOVIE_UPDATE.success, API_MOVIE_CREATE.success];

function* setErrors(action) {
  const { payload } = action;
  const { json } = payload;
  const errors = json?.errors;

  yield setFormError(FORM_MOVIE, errors);
}

function* redirectToMovie(action) {
  const { payload } = action;
  const { json } = payload;
  const id = json?.movie?.id;

  yield put(push(`/movies/${id}`));
}

function* redirectHome() {
  yield put(push('/'));
}

function* refetchFacets() {
  yield put(apiFacetsFetch());
}

function* moviesSaga() {
  yield takeEvery(successActions, redirectToMovie);
  yield takeEvery(redirectErrorActions, redirectHome);
  yield takeEvery(validationErrorActions, setErrors);
  yield takeEvery(API_MOVIE_RATE.success, refetchFacets);
}

export { moviesSaga };
