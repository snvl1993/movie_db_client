import React from 'react';
import map from 'lodash/map';
import find from 'lodash/find';
import isEqual from 'lodash/isEqual';
import { useDispatch, useSelector } from 'react-redux';

import {
  formInit,
  formUnmount,
  formSetFormError,
  formSetError,
  formSetValue,
  formSetValues
} from './actions';
import { selectForm } from './selectors';
import { validate, validateField } from './utils/helpers';

const useForm = (form, params = {}) => {
  const { defaults = {}, persist = false, validations = {}, onSubmit } = params;

  const dispatch = useDispatch();
  const [defaultValues] = React.useState(defaults);
  const [touched, setTouched] = React.useState(false);
  const [submitted, setSubmitted] = React.useState(false);
  const [valid, setValid] = React.useState(true);

  const data = useSelector((state) => selectForm(state, form, defaultValues));
  const { formError, errors, values } = data;

  const setFieldErrors = (messages, field) => {
    const fieldErrors = errors[field];
    if (!isEqual(messages, fieldErrors)) {
      dispatch(formSetError(form, field, messages));
    }
  };

  const setValue = (field) => (value) => {
    if (formError) dispatch(formSetFormError(form, null));

    setTouched(true);
    dispatch(formSetValue(form, field, value));

    const messages = validateField(field, value, validations);
    setFieldErrors(messages, field);

    if (messages?.length && valid) setValid(false);
  };

  const resetValues = (values) => {
    dispatch(formSetValues(form, values));
  };

  const submit = (e) => {
    e.preventDefault();
    setSubmitted(true);

    const messages = validate(values, validations);
    map(messages, setFieldErrors);
    if (find(messages, 'length')) return;

    onSubmit(values, e);
  };

  React.useEffect(() => {
    dispatch(formInit(form, defaultValues));

    return () => {
      if (!persist) dispatch(formUnmount(form));
    };
  }, [form, persist, dispatch, defaultValues]);

  return {
    formError,
    errors,
    values,
    setValue,
    resetValues,
    submit,
    touched,
    submitted,
    valid
  };
};

export { useForm };
