import {
  FORM_INIT,
  FORM_UNMOUNT,
  FORM_SET_VALUE,
  FORM_SET_VALUES,
  FORM_SET_ERROR,
  FORM_SET_FORM_ERROR
} from './constants';

export const FORM_EMPTY = {
  formError: null,
  values: {},
  errors: {}
};

const initialState = {};

const reducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case FORM_INIT: {
      const { form, values = {} } = payload;
      const initialized = { ...FORM_EMPTY, values };
      const newForm = state[form] ? state[form] : initialized;

      return {
        ...state,
        [form]: newForm
      };
    }

    case FORM_UNMOUNT: {
      const { form } = payload;
      const newState = { ...state };
      delete newState[form];
      return newState;
    }

    case FORM_SET_VALUE: {
      const { form, field, value } = payload;

      return {
        ...state,
        [form]: {
          ...state[form],
          values: {
            ...state[form].values,
            [field]: value
          }
        }
      };
    }

    case FORM_SET_VALUES: {
      const { form, values } = payload;

      return {
        ...state,
        [form]: {
          ...state[form],
          values
        }
      };
    }

    case FORM_SET_ERROR: {
      const { form, field, error } = payload;

      return {
        ...state,
        [form]: {
          ...state[form],
          errors: {
            ...state[form].errors,
            [field]: error
          }
        }
      };
    }

    case FORM_SET_FORM_ERROR: {
      const { form, error } = payload;

      return {
        ...state,
        [form]: {
          ...state[form],
          formError: error
        }
      };
    }

    default:
      return state;
  }
};

export default reducer;
