import map from 'lodash/map';
import { put, all } from 'redux-saga/effects';

import { formSetFormError, formSetError } from '@form/actions';

function* setFormError(formName, errors = {}) {
  const { base, ...restErrors } = errors;

  const mapper = (error, field) => put(formSetError(formName, field, error));
  const effects = map(restErrors, mapper);

  yield put(formSetFormError(formName, base));
  yield all(effects);
}

export { setFormError };
