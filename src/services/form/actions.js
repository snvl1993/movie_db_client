import {
  FORM_INIT,
  FORM_SET_VALUE,
  FORM_SET_VALUES,
  FORM_SET_ERROR,
  FORM_SET_FORM_ERROR,
  FORM_UNMOUNT
} from './constants';

const formInit = (form, values) => ({
  type: FORM_INIT,
  payload: { form, values }
});

const formSetValue = (form, field, value) => ({
  type: FORM_SET_VALUE,
  payload: { form, field, value }
});

const formSetValues = (form, values) => ({
  type: FORM_SET_VALUES,
  payload: { form, values }
});

const formSetError = (form, field, error) => ({
  type: FORM_SET_ERROR,
  payload: { form, field, error }
});

const formSetFormError = (form, error) => ({
  type: FORM_SET_FORM_ERROR,
  payload: { form, error }
});

const formUnmount = (form) => ({
  type: FORM_UNMOUNT,
  payload: { form }
});

export { formInit, formSetValue, formSetValues,formSetError, formSetFormError, formUnmount };
