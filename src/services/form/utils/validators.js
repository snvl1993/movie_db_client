import isNull from 'lodash/isNull';

import { isEmpty, isEmail, isGreater, isLess, isInRange } from './helpers';

const validatePresence = (value) =>
  isEmpty(value) ? 'should not be empty' : null;

const validateEmail = (value) =>
  isEmail(value) ? null : 'should be a valid email address';

const validateGreaterThan = (min) => (value) =>
  isGreater(value, min) ? null : `should be greater than ${min}`;

const validateLessThan = (max) => (value) =>
  isLess(value, max) ? null : `should be less than ${max}`;

const validateRange = ({ min = null, max = null }) => (value) => {
  if (!isInRange(value, { min, max })) {
    if (!isNull(min) && !isNull(max)) {
      return `should be between ${min} and ${max}`;
    } else if (!isNull(min)) {
      return `should be above ${min}`;
    }

    return `should be below ${max}`;
  }

  return null;
};

export {
  validatePresence,
  validateEmail,
  validateRange,
  validateGreaterThan,
  validateLessThan
};
