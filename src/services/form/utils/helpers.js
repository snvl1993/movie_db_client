import mapValues from 'lodash/mapValues';
import isNull from 'lodash/isNull';

const isEmpty = (value) => !value;
const isEmail = (value) => /.+@.+\..+/.test(value);
const isGreater = (value, min) => value > min;
const isLess = (value, max) => value < max;
const isInRange = (value, { min = null, max = null }) => {
  if (isNull(value)) return true;
  if (!isNull(max) && value > max) return false;
  return isNull(min) || value >= min;
};

const validateField = (field, value, validations) => {
  const { [field]: validators } = validations;
  if (!validators?.length) return [];

  const reducer = (acc, validator) => {
    const message = validator(value);
    return message ? [...acc, message] : acc;
  };

  return validators.reduce(reducer, []);
};

const validate = (values, validations) =>
  mapValues(validations, (_, field) => {
    const { [field]: value } = values;
    return validateField(field, value, validations);
  });

export {
  isEmpty,
  isEmail,
  isGreater,
  isLess,
  isInRange,
  validate,
  validateField
};
