import get from 'lodash/get';
import { createSelector } from 'reselect';

import { FORM_EMPTY } from './reducer';

const selectForms = (state) => get(state, 'form', {});

const selectForm = createSelector(
  selectForms,
  (state, form) => form,
  (state, form, defaults) => defaults,
  (state, form, defaults) =>
    get(state, form, { ...FORM_EMPTY, values: defaults })
);

const selectFormErrors = createSelector(selectForm, (state) =>
  get(state, 'errors', {})
);

const selectFormValues = createSelector(selectForm, (state) =>
  get(state, 'values', {})
);

export { selectForms, selectForm, selectFormErrors, selectFormValues };
