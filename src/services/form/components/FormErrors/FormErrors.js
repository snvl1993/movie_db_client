import React from 'react';
import isArray from 'lodash/isArray';

const FormErrors = (props) => {
  const { className, errors } = props;

  if (!errors || !errors.length) return null;
  const messages = isArray(errors) ? errors : [errors];

  const renderError = (error) => (
    <div className="ui-error" key={error}>
      {error}
    </div>
  );

  return <div className={className}>{messages.map(renderError)}</div>;
};

export { FormErrors };
