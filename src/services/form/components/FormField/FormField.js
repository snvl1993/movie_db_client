import React from 'react';

import { FormErrors } from '@form/components/FormErrors';

const FormField = (props) => {
  const { className, error, name, label, children } = props;

  return (
    <div className={className}>
      {label && <label htmlFor={name}>{label}</label>}
      {children}
      <FormErrors errors={error} />
    </div>
  );
};

export { FormField };
