import React from 'react';

import { FormField } from '@form/components/FormField';

const FormInput = (props) => {
  const {
    className,
    error,
    name,
    label,
    onChange,
    type,
    ...rest
  } = props;

  const Component = type === 'textarea' ? 'textarea' : 'input';

  const onChangeHandler = (e) => onChange(e.target.value, e);

  return (
    <FormField className={className} error={error} name={name} label={label}>
      <Component {...rest} name={name} id={name} type={type} onChange={onChangeHandler} />
    </FormField>
  );
};

FormInput.defaultProps = {
  type: 'text',
  value: ''
};

export { FormInput };
