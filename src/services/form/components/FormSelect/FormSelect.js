import React from 'react';

import { FormField } from '@form/components/FormField';

const FormSelect = (props) => {
  const {
    className,
    error,
    name,
    label,
    options,
    onChange,
    multiple,
    ...rest
  } = props;

  const onChangeHandler = (e) => {
    if (multiple) {
      const values = [...e.target.selectedOptions].map((o) => o.value);
      onChange(values, e);
    } else {
      onChange(e.target.value, e);
    }
  };

  const renderOptions = (option) => {
    const { value, label } = option;

    return (
      <option value={value} key={value}>
        {label}
      </option>
    );
  };

  return (
    <FormField className={className} error={error} name={name} label={label}>
      <select
        {...rest}
        name={name}
        id={name}
        onChange={onChangeHandler}
        multiple={multiple}
      >
        {options.map(renderOptions)}
      </select>
    </FormField>
  );
};

FormSelect.defaultProps = {
  value: ''
};

export { FormSelect };
