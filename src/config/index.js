const config = {
  environment: process.env.NODE_ENV
};

export default config;
