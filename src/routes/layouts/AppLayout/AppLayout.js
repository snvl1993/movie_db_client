import React from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { apiSignOut } from '@services/api/actions/auth';
import { selectUser } from '@services/user/selectors';

import styles from './styles.module.css';

const AppLayout = (props) => {
  const { children } = props;

  const dispatch = useDispatch();
  const user = useSelector(selectUser);

  const addBodyClass = () => document.body.classList.add(styles.body);
  const removeBodyClass = () => document.body.classList.remove(styles.body);

  React.useEffect(() => {
    addBodyClass();
    return removeBodyClass;
  }, []);

  const logOut = () => dispatch(apiSignOut());

  const renderWelcome = () => {
    if (!user) return null;

    const { email } = user;
    return <span>Welcome {email}!</span>;
  };

  const renderAuthLinks = () => {
    if (user) {
      return (
        <button className={styles.button} onClick={logOut} type="button">
          Log Out
        </button>
      );
    }

    return (
      <div className={styles.authLinks}>
        <Link to="/auth/sign-in">Sign In</Link>
        <Link to="/auth/sign-up">Sign Up</Link>
      </div>
    );
  };

  return (
    <div className={styles.component}>
      <div className={styles.header}>
        <div className="ui-container">
          <Link className={styles.home} to="/">
            Home
          </Link>

          {renderWelcome()}
          {renderAuthLinks()}
        </div>
      </div>

      <div className={styles.content}>{children}</div>
    </div>
  );
};

export { AppLayout };
