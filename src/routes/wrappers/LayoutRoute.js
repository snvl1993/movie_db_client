import React from 'react';
import { Route } from 'react-router-dom';

const LayoutRoute = ({
  component: Component,
  layout: Layout,
  render,
  ...rest
}) => (
  <Route
    {...rest}
    render={(matchProps) => (
      <Layout>
        {Component && <Component {...matchProps} />}
        {render && render(matchProps)}
      </Layout>
    )}
  />
);

export { LayoutRoute };
