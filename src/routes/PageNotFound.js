import React from 'react';

const PageNotFound = () => (
  <div className="ui-section ui-container">
    <h1 className="ui-h1">Page not found</h1>
  </div>
);

export { PageNotFound };
