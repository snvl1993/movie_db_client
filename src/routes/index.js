import React from 'react';
import { Switch } from 'react-router-dom';

import { AppLayout } from '@src/routes/layouts/AppLayout';
import { LayoutRoute } from '@src/routes/wrappers/LayoutRoute';
import { PageHome } from '@services/homepage/pages/Async';
import { PageSignIn, PageSignUp } from '@services/auth/pages/Async';
import {
  PageMovieEdit,
  PageMovieCreate,
  PageMovieShow
} from '@services/movies/pages/Async';
import { PageNotFound } from '@src/routes/PageNotFound';

const AppLayoutRoute = (props) => <LayoutRoute layout={AppLayout} {...props} />;

export default () => (
  <Switch>
    <AppLayoutRoute exact path="/" component={PageHome} />
    <AppLayoutRoute exact path="/auth/sign-in" component={PageSignIn} />
    <AppLayoutRoute exact path="/auth/sign-up" component={PageSignUp} />
    <AppLayoutRoute exact path="/movies/new" component={PageMovieCreate} />
    <AppLayoutRoute exact path="/movies/:id" component={PageMovieShow} />
    <AppLayoutRoute exact path="/movies/:id/edit" component={PageMovieEdit} />
    <AppLayoutRoute component={PageNotFound} />
  </Switch>
);
