import React, { Suspense } from 'react';
import { ConnectedRouter } from 'connected-react-router';
import { Provider } from 'react-redux';
import { createBrowserHistory } from 'history';

import routes from '@src/routes';
import createStore from '@store/createStore';
import { UserProvider } from '@services/user/components/UserProvider';

import '@styles/global.css';

const initialState = {};
const history = createBrowserHistory();
const store = createStore(initialState, history);
store.runSaga();

function App() {
  const renderLoading = () => <div>Loading...</div>;

  return (
    <Provider store={store}>
      <Suspense fallback={renderLoading}>
        <ConnectedRouter history={history}>
          <UserProvider>{routes()}</UserProvider>
        </ConnectedRouter>
      </Suspense>
    </Provider>
  );
}

export { App };
