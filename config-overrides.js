module.exports = function override(config, env) {
  config.resolve.alias = {
    ...config.resolve.alias,
    '@src': `${__dirname}/src`,
    '@config': '@src/config',
    '@store': '@src/store',
    '@lib': '@src/lib',
    '@styles': '@src/styles',
    '@global': '@src/global',
    '@services': '@src/services',
    '@form': '@services/form',
    '@api': '@services/api'
  };

  return config;
};
